from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext


def index(request):
    "Main page"

    return render_to_response('base/base_site.html', {
            'foo': 'bar',
    }, context_instance=RequestContext(request))
