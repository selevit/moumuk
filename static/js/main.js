function display_hide(block_id) {
    if ($(block_id).css('display') == 'none') {
        $(block_id).animate({height: 'show'}, 0);
    } else {
        $(block_id).animate({height: 'hide'}, 0);
    }
}
function display(block_id) {
    $(block_id).animate({height: 'show'}, 500);
}
function hide(block_id) {
    $(block_id).animate({height: 'hide'}, 250);
}
