#! -*- coding: utf-8 -*-

from  testing.models import *
from django.contrib import admin

class SchoolClassAdmin(admin.ModelAdmin):
    "Добавление и редактирование классов"

    fieldsets = [
        ('Основная информация',
         {'fields': ['number', 'char_item', 'school', 'head']}),
    ]
    list_display = ('class_name_with_char_item', 'school', 'head')
    list_filter = ['number', 'school', 'head']

class SchoolInline(admin.TabularInline):
    '''
       Быстрое добавление и редактирование классов
       при добавлении или редактировании школ
    '''

    model = SchoolClass
    extra = 3

class SchoolAdmin(admin.ModelAdmin):
    "Добавление и редактирование школ"

    fieldsets = [
       ('Основная информация', 
        {'fields': ['number', 'full_name', 'director']}),
       ('Контактная информация',
        {'fields': ['address', 'phone', 'email']}),
       ('Дополнительная информация',
        {'fields': ['site', 'desc'], 'classes': ['collapse']}),
    ]
    inlines = [SchoolInline]
    list_display = ('number', 'director', 'phone', 'email')
    search_fields = ['number', 'director']

class GroupAdmin(admin.ModelAdmin):
    "Добавление и редактирование групп МОУМУК"

    fieldsets = [
        ('Основная информация',
         {'fields': ['name', 'profile']}),
        ('Дополнительная информация',
         {'fields': ['desc'], 'classes': ['collapse']}),
    ]
    list_display = ('name', 'profile')
    list_filter = ['profile']
    search_fields = ['name']

class ProfileGroupInline(admin.TabularInline):
    '''
        Быстрое добавление и редактирование групп
        при добавлении или редактировании профилей
    '''

    model = Group
    extra = 1

class ProfileSubjectInline(admin.TabularInline):
    '''
        Быстрое добавление и редактирование предметов
        при добавлении или редактировании профилей
    '''
    model = Subject
    extra = 1

class ProfileAdmin(admin.ModelAdmin):
    "Добавление и редактирование профилей"

    fieldsets = [
        ('Основная информация',
         {'fields': ['name', 'chair', 'head']}),
        ('Дополнительная информация',
         {'fields': ['desc'], 'classes': ['collapse']}),
    ]
    inlines = [ProfileGroupInline, ProfileSubjectInline]
    list_display = ('name', 'head')
    list_filter = ['chair']
    search_fields = ['name']

class ChairInline(admin.TabularInline):
    '''
        Быстрое добаление и редактирование профилей
        при добавлении или редактировании кафедр
    '''

    model = Profile
    extra = 1

class ChairAdmin(admin.ModelAdmin):
    "Добавление и редактирование кафедр"

    fieldsets = [
        ('Основная информация',
         {'fields': ['name', 'head']}),
    ]
    inlines = [ChairInline]
    list_display = ('name', 'head')
    search_fields = ['name']

class TestHardLevelAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Основная информаци', {'fields': ['name']}),
        ('Дополнительная информации', {'fields': ['desc']}),
    ]

class StudentsTestInline(admin.TabularInline):
    "быстрое добавление вопросов при создании теста"

    model = Question
    extra = 4

class StudentsTestAdmin(admin.ModelAdmin):
    "Сами тесты"

    fieldsets = [
        ('Основная информация',
         {'fields': ['name', 'subject',
                     'school_class', 'hard_level', 'time_limit']}),
        ('Дополнительная информация',
         {'fields': ['desc'], 'classes': 'collapse'}),
    ]
    list_display = ('name', 'subject',
                    'school_class', 'hard_level', 'time_limit')
    list_filter = ['school_class', 'subject', 'hard_level']
    search_fields = ['name', 'desc']
    inlines = [StudentsTestInline]
    

class QuestionInline(admin.TabularInline):
    '''
        Добаление и редактирование вариантов ответов
        при добавлении или редактировании тестовых вопросов
    '''

    model = Answer
    extra = 3

class QuestionAdmin(admin.ModelAdmin):
    "Добавление и редактирование тестовых вопросов"

    fieldsets = [
        ('Вопрос',
         {'fields': ['text', 'test', 'ball', 'order', 'image']}),
    ]
    list_display = ('test', 'text', 'ball')
    list_filter = ['ball', 'test']
    search_fields = ['text']
    inlines = [QuestionInline]

class SubjectAdmin(admin.ModelAdmin):
    "Добавление или редактирование дисциплин"

    fieldsets = [
        ('Основная информация',
         {'fields': ['name', 'profile']}),
        ('Дополнительная информация',
         {'fields': ['desc'], 'classes': ['collapse']}),
    ]
    list_display = ('name', 'profile')
    list_filter = ['profile']
    search_fields = ['name']
    
class TeacherAdmin(admin.ModelAdmin):
    '''
        Добавление и редактирование преподователей.
        Необходимы полномочия администратора
    '''

    fieldsets = [
        ('Основная информация',
         {'fields': ['last_name', 'first_name', 'middle_name']}),
        ('Контактная информация',
         {'fields': ['phone']}),
        ('Дополнительная информация',
         {'fields': ['desc'], 'classes': ['collapse']}),
    ]
    list_display = ('teacher_short_fullname', 'phone')
    search_fields = ['last_name', 'first_name', 'middle_name']

class StudentGroupInline(admin.TabularInline):
    '''
        Писвоение и удаление групп студентам
        при добавлении или редактировании студентов
    '''
    model = StudentGroup
    extra = 1

class StudentAdmin(admin.ModelAdmin):
    "Добавление и редактирование студентов"

    fieldsets = [
        ('Основная информация',
         {'fields': ['last_name', 'first_name', 
                     'middle_name', 'school', 'school_class']}),
    ]
    inlines = [StudentGroupInline]
    list_display = ('last_name', 'first_name', 'middle_name', 'school', 'school_class')
    list_filter = ['school', 'school_class']
    search_fields = ['last_name', 'first_name', 'middle_name']

class QuestionLogAdmin(admin.ModelAdmin):
    "Подробная история тестирования"

    fieldsets = [
        ('Основная информация',
         {'fields': ['student', 'test', 'question', 'selected']}),
    ]
    list_display = ('student', 'test', 'question', 'selected')

class TestLogAdmin(admin.ModelAdmin):
    "Журнал тестирования"

    fieldsets = [
        ('Основная информация',
         {'fields': ['student', 'test', 'correct',
            'incorrect', 'ball']}),
    ]
    list_display = ('student', 'test', 'correct', 'incorrect', 'ball')

admin.site.register(StudentsTest, StudentsTestAdmin)
admin.site.register(TestHardLevel, TestHardLevelAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Chair, ChairAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(School, SchoolAdmin)
admin.site.register(SchoolClass, SchoolClassAdmin)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(QuestionLog, QuestionLogAdmin)
admin.site.register(TestLog, TestLogAdmin)
