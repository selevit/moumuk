# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'School'
        db.create_table('testing_school', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('director', self.gf('django.db.models.fields.CharField')(max_length=90)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phone', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('site', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('testing', ['School'])

        # Adding model 'Teacher'
        db.create_table('testing_teacher', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('middle_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('phone', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('testing', ['Teacher'])

        # Adding model 'SchoolClass'
        db.create_table('testing_schoolclass', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('char_item', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('school', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.School'])),
            ('head', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Teacher'])),
        ))
        db.send_create_signal('testing', ['SchoolClass'])

        # Adding model 'Student'
        db.create_table('testing_student', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('middle_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('school', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.School'])),
            ('school_class', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.SchoolClass'])),
        ))
        db.send_create_signal('testing', ['Student'])

        # Adding model 'Chair'
        db.create_table('testing_chair', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('head', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Teacher'])),
        ))
        db.send_create_signal('testing', ['Chair'])

        # Adding model 'Profile'
        db.create_table('testing_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('chair', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Chair'])),
            ('head', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Teacher'])),
            ('desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('testing', ['Profile'])

        # Adding model 'Group'
        db.create_table('testing_group', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Profile'])),
            ('desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('testing', ['Group'])

        # Adding model 'StudentGroup'
        db.create_table('testing_studentgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Student'])),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Group'])),
        ))
        db.send_create_signal('testing', ['StudentGroup'])

        # Adding model 'Subject'
        db.create_table('testing_subject', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Profile'])),
            ('desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('testing', ['Subject'])

        # Adding model 'TestHardLevel'
        db.create_table('testing_testhardlevel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('testing', ['TestHardLevel'])

        # Adding model 'StudentsTest'
        db.create_table('testing_studentstest', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('school_class', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('hard_level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.TestHardLevel'])),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Subject'])),
            ('desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('testing', ['StudentsTest'])

        # Adding model 'Question'
        db.create_table('testing_question', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.StudentsTest'])),
            ('order', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('ball', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('testing', ['Question'])

        # Adding model 'Answer'
        db.create_table('testing_answer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Question'])),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('order', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('is_correct', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('testing', ['Answer'])

        # Adding model 'TestLog'
        db.create_table('testing_testlog', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Student'])),
            ('test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.StudentsTest'])),
            ('testing_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('correct', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('incorrect', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('ball', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('trial_num', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal('testing', ['TestLog'])

        # Adding model 'QuestionLog'
        db.create_table('testing_questionlog', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Student'])),
            ('test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.StudentsTest'])),
            ('trial_num', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Question'])),
            ('selected', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Answer'])),
            ('submit_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('testing', ['QuestionLog'])

    def backwards(self, orm):
        # Deleting model 'School'
        db.delete_table('testing_school')

        # Deleting model 'Teacher'
        db.delete_table('testing_teacher')

        # Deleting model 'SchoolClass'
        db.delete_table('testing_schoolclass')

        # Deleting model 'Student'
        db.delete_table('testing_student')

        # Deleting model 'Chair'
        db.delete_table('testing_chair')

        # Deleting model 'Profile'
        db.delete_table('testing_profile')

        # Deleting model 'Group'
        db.delete_table('testing_group')

        # Deleting model 'StudentGroup'
        db.delete_table('testing_studentgroup')

        # Deleting model 'Subject'
        db.delete_table('testing_subject')

        # Deleting model 'TestHardLevel'
        db.delete_table('testing_testhardlevel')

        # Deleting model 'StudentsTest'
        db.delete_table('testing_studentstest')

        # Deleting model 'Question'
        db.delete_table('testing_question')

        # Deleting model 'Answer'
        db.delete_table('testing_answer')

        # Deleting model 'TestLog'
        db.delete_table('testing_testlog')

        # Deleting model 'QuestionLog'
        db.delete_table('testing_questionlog')

    models = {
        'testing.answer': {
            'Meta': {'object_name': 'Answer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_correct': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Question']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'testing.chair': {
            'Meta': {'object_name': 'Chair'},
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Teacher']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'testing.group': {
            'Meta': {'object_name': 'Group'},
            'desc': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Profile']"})
        },
        'testing.profile': {
            'Meta': {'object_name': 'Profile'},
            'chair': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Chair']"}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Teacher']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'testing.question': {
            'Meta': {'object_name': 'Question'},
            'ball': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.StudentsTest']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        'testing.questionlog': {
            'Meta': {'object_name': 'QuestionLog'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Question']"}),
            'selected': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Answer']"}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Student']"}),
            'submit_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.StudentsTest']"}),
            'trial_num': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'testing.school': {
            'Meta': {'object_name': 'School'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            'director': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'phone': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'site': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'testing.schoolclass': {
            'Meta': {'object_name': 'SchoolClass'},
            'char_item': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Teacher']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'school': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.School']"})
        },
        'testing.student': {
            'Meta': {'object_name': 'Student'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'school': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.School']"}),
            'school_class': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.SchoolClass']"})
        },
        'testing.studentgroup': {
            'Meta': {'object_name': 'StudentGroup'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Student']"})
        },
        'testing.studentstest': {
            'Meta': {'object_name': 'StudentsTest'},
            'desc': ('django.db.models.fields.TextField', [], {}),
            'hard_level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.TestHardLevel']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'school_class': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Subject']"})
        },
        'testing.subject': {
            'Meta': {'object_name': 'Subject'},
            'desc': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Profile']"})
        },
        'testing.teacher': {
            'Meta': {'object_name': 'Teacher'},
            'desc': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'phone': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'testing.testhardlevel': {
            'Meta': {'object_name': 'TestHardLevel'},
            'desc': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'testing.testlog': {
            'Meta': {'object_name': 'TestLog'},
            'ball': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'correct': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incorrect': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Student']"}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.StudentsTest']"}),
            'testing_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trial_num': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        }
    }

    complete_apps = ['testing']