# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Question.image'
        db.add_column('testing_question', 'image',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True),
                      keep_default=False)


        # Changing field 'SchoolClass.head'
        db.alter_column('testing_schoolclass', 'head_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['testing.Teacher'], null=True))

        # Changing field 'School.site'
        db.alter_column('testing_school', 'site', self.gf('django.db.models.fields.URLField')(max_length=200, null=True))

        # Changing field 'School.phone'
        db.alter_column('testing_school', 'phone', self.gf('django.db.models.fields.PositiveIntegerField')(null=True))

        # Changing field 'School.address'
        db.alter_column('testing_school', 'address', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'School.email'
        db.alter_column('testing_school', 'email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True))

        # Changing field 'School.desc'
        db.alter_column('testing_school', 'desc', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'TestHardLevel.desc'
        db.alter_column('testing_testhardlevel', 'desc', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'StudentsTest.desc'
        db.alter_column('testing_studentstest', 'desc', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Group.desc'
        db.alter_column('testing_group', 'desc', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Subject.desc'
        db.alter_column('testing_subject', 'desc', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Profile.desc'
        db.alter_column('testing_profile', 'desc', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Teacher.phone'
        db.alter_column('testing_teacher', 'phone', self.gf('django.db.models.fields.PositiveIntegerField')(null=True))

        # Changing field 'Teacher.desc'
        db.alter_column('testing_teacher', 'desc', self.gf('django.db.models.fields.TextField')(null=True))
    def backwards(self, orm):
        # Deleting field 'Question.image'
        db.delete_column('testing_question', 'image')


        # User chose to not deal with backwards NULL issues for 'SchoolClass.head'
        raise RuntimeError("Cannot reverse this migration. 'SchoolClass.head' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'School.site'
        raise RuntimeError("Cannot reverse this migration. 'School.site' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'School.phone'
        raise RuntimeError("Cannot reverse this migration. 'School.phone' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'School.address'
        raise RuntimeError("Cannot reverse this migration. 'School.address' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'School.email'
        raise RuntimeError("Cannot reverse this migration. 'School.email' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'School.desc'
        raise RuntimeError("Cannot reverse this migration. 'School.desc' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'TestHardLevel.desc'
        raise RuntimeError("Cannot reverse this migration. 'TestHardLevel.desc' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'StudentsTest.desc'
        raise RuntimeError("Cannot reverse this migration. 'StudentsTest.desc' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Group.desc'
        raise RuntimeError("Cannot reverse this migration. 'Group.desc' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Subject.desc'
        raise RuntimeError("Cannot reverse this migration. 'Subject.desc' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Profile.desc'
        raise RuntimeError("Cannot reverse this migration. 'Profile.desc' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Teacher.phone'
        raise RuntimeError("Cannot reverse this migration. 'Teacher.phone' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Teacher.desc'
        raise RuntimeError("Cannot reverse this migration. 'Teacher.desc' and its values cannot be restored.")
    models = {
        'testing.answer': {
            'Meta': {'object_name': 'Answer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_correct': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Question']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'testing.chair': {
            'Meta': {'object_name': 'Chair'},
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Teacher']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'testing.group': {
            'Meta': {'object_name': 'Group'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Profile']"})
        },
        'testing.profile': {
            'Meta': {'object_name': 'Profile'},
            'chair': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Chair']"}),
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Teacher']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'testing.question': {
            'Meta': {'object_name': 'Question'},
            'ball': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.StudentsTest']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        'testing.questionlog': {
            'Meta': {'object_name': 'QuestionLog'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Question']"}),
            'selected': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Answer']"}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Student']"}),
            'submit_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.StudentsTest']"}),
            'trial_num': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'testing.school': {
            'Meta': {'object_name': 'School'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'director': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'phone': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'testing.schoolclass': {
            'Meta': {'object_name': 'SchoolClass'},
            'char_item': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Teacher']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'school': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.School']"})
        },
        'testing.student': {
            'Meta': {'object_name': 'Student'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'school': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.School']"}),
            'school_class': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.SchoolClass']"})
        },
        'testing.studentgroup': {
            'Meta': {'object_name': 'StudentGroup'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Student']"})
        },
        'testing.studentstest': {
            'Meta': {'object_name': 'StudentsTest'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'hard_level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.TestHardLevel']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'school_class': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Subject']"})
        },
        'testing.subject': {
            'Meta': {'object_name': 'Subject'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Profile']"})
        },
        'testing.teacher': {
            'Meta': {'object_name': 'Teacher'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'phone': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'testing.testhardlevel': {
            'Meta': {'object_name': 'TestHardLevel'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'testing.testlog': {
            'Meta': {'object_name': 'TestLog'},
            'ball': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'correct': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incorrect': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Student']"}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.StudentsTest']"}),
            'testing_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'trial_num': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        }
    }

    complete_apps = ['testing']