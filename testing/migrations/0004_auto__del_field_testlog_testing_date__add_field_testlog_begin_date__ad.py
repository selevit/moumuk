# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'TestLog.testing_date'
        db.delete_column('testing_testlog', 'testing_date')

        # Adding field 'TestLog.begin_date'
        db.add_column('testing_testlog', 'begin_date',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, null=True, blank=True),
                      keep_default=False)

        # Adding field 'TestLog.finish_date'
        db.add_column('testing_testlog', 'finish_date',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)


        # Changing field 'TestLog.incorrect'
        db.alter_column('testing_testlog', 'incorrect', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True))

        # Changing field 'TestLog.ball'
        db.alter_column('testing_testlog', 'ball', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True))

        # Changing field 'TestLog.correct'
        db.alter_column('testing_testlog', 'correct', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True))
    def backwards(self, orm):
        # Adding field 'TestLog.testing_date'
        db.add_column('testing_testlog', 'testing_date',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=0, blank=True),
                      keep_default=False)

        # Deleting field 'TestLog.begin_date'
        db.delete_column('testing_testlog', 'begin_date')

        # Deleting field 'TestLog.finish_date'
        db.delete_column('testing_testlog', 'finish_date')


        # Changing field 'TestLog.incorrect'
        db.alter_column('testing_testlog', 'incorrect', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0))

        # Changing field 'TestLog.ball'
        db.alter_column('testing_testlog', 'ball', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0))

        # Changing field 'TestLog.correct'
        db.alter_column('testing_testlog', 'correct', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0))
    models = {
        'testing.answer': {
            'Meta': {'object_name': 'Answer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_correct': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Question']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'testing.chair': {
            'Meta': {'object_name': 'Chair'},
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Teacher']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'testing.group': {
            'Meta': {'object_name': 'Group'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Profile']"})
        },
        'testing.profile': {
            'Meta': {'object_name': 'Profile'},
            'chair': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Chair']"}),
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Teacher']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'testing.question': {
            'Meta': {'object_name': 'Question'},
            'ball': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.StudentsTest']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        'testing.questionlog': {
            'Meta': {'object_name': 'QuestionLog'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Question']"}),
            'selected': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Answer']"}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Student']"}),
            'submit_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.StudentsTest']"}),
            'trial_num': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'testing.school': {
            'Meta': {'object_name': 'School'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'director': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'phone': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'testing.schoolclass': {
            'Meta': {'object_name': 'SchoolClass'},
            'char_item': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'head': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Teacher']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'school': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.School']"})
        },
        'testing.student': {
            'Meta': {'object_name': 'Student'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'school': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.School']"}),
            'school_class': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.SchoolClass']"})
        },
        'testing.studentgroup': {
            'Meta': {'object_name': 'StudentGroup'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Student']"})
        },
        'testing.studentstest': {
            'Meta': {'object_name': 'StudentsTest'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'hard_level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.TestHardLevel']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'school_class': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Subject']"}),
            'time_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '20'})
        },
        'testing.subject': {
            'Meta': {'object_name': 'Subject'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Profile']"})
        },
        'testing.teacher': {
            'Meta': {'object_name': 'Teacher'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'phone': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'testing.testhardlevel': {
            'Meta': {'object_name': 'TestHardLevel'},
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'testing.testlog': {
            'Meta': {'object_name': 'TestLog'},
            'ball': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'begin_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'correct': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'finish_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incorrect': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.Student']"}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['testing.StudentsTest']"}),
            'trial_num': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        }
    }

    complete_apps = ['testing']