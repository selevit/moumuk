#!-*- coding: utf-8 -*-

from django.db import models

class School(models.Model):
    "Информация о школах, сотрудничающих с МОУМУК"
    
    number = models.PositiveSmallIntegerField('Номер школы')
    full_name = models.CharField('Полное название', max_length=150)
    director = models.CharField('Директор', max_length=90)
    address = models.CharField('Адрес', max_length=255, null=True, blank=True)
    phone = models.PositiveIntegerField('Телефон', null=True, blank=True)
    email = models.EmailField('Email', null=True, blank=True)
    site = models.URLField('Сайт', null=True, blank=True)
    desc = models.TextField('Описание', null=True, blank=True)
    
    def __unicode__(self):
        return str(self.number)

    class Meta:
        verbose_name = "школу"
        verbose_name_plural = "Школы"

class Teacher(models.Model):
    "Информация о преподавателях школ и МОУМУК"

    last_name = models.CharField('Фамилия', max_length=30)
    first_name = models.CharField('Имя', max_length=30)
    middle_name = models.CharField('Отчество', max_length=30)
    phone = models.PositiveIntegerField('Телефон', null=True, blank=True)
    desc = models.TextField('Краткая информация', null=True, blank=True)
    
    def teacher_short_fullname(self):
        return self.last_name + ' ' + self.first_name[:1] + '.' + self.middle_name[:1] + '.'
    teacher_short_fullname.short_description = 'Преподаватель'

    def __unicode__(self):
        return self.teacher_short_fullname()

    class Meta:
        verbose_name = "преподавателя"
        verbose_name_plural = "Преподаватели"
    
class SchoolClass(models.Model):
    "Информация о классах школ, сотрудничающих с МОУМУК"

    number = models.PositiveSmallIntegerField('Номер класса')
    char_item = models.CharField('Буква класса', max_length=1)
    school = models.ForeignKey(School, verbose_name='Школа')
    head = models.ForeignKey(Teacher, verbose_name='Классный руководитель', null=True, blank=True)

    def class_name_with_char_item(self):
        return str(self.number) + self.char_item
    class_name_with_char_item.short_description = 'Класс'

    def __unicode__(self):
        return self.class_name_with_char_item()

    class Meta:
        verbose_name = "класс"
        verbose_name_plural = "Классы"

class Student(models.Model):
    "Информация о студентах МОУМУК"

    last_name = models.CharField('Фамилия', max_length=30)
    first_name = models.CharField('Имя', max_length=30)
    middle_name = models.CharField('Отчество', max_length=30)
    school = models.ForeignKey(School, verbose_name='Школа')
    school_class = models.ForeignKey(SchoolClass, verbose_name='Класс')

    def __unicode__(self):
        return self.last_name + ' ' + self.first_name + ' ' + self.middle_name

    class Meta:
        verbose_name = "ученика"
        verbose_name_plural = "Ученики"

class Chair(models.Model):
    "Кафедры МОУМУК"

    name = models.CharField('Название кафедры', max_length=100)
    head = models.ForeignKey(Teacher, verbose_name='Заведующий')
    
    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "кафедру"
        verbose_name_plural = "Кафедры"

class Profile(models.Model):
    "Учебные профили МОУМУК"

    name = models.CharField('Название профиля', max_length=50)
    chair = models.ForeignKey(Chair, verbose_name='Кафедра')
    head = models.ForeignKey(Teacher, verbose_name='Заведующий профилем')
    desc = models.TextField('Описание', null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "профиль"
        verbose_name_plural = "Профили"

class Group(models.Model):
    "Информация о группах МОУМУК"

    name = models.CharField('Название', max_length=50)
    profile = models.ForeignKey(Profile, verbose_name='Профиль')
    desc = models.TextField('Описание', null=True, blank=True)
    
    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "группу"
        verbose_name_plural = "Группы"

class StudentGroup(models.Model):
    "Информация о группах каждого студента МОУМУК"

    student = models.ForeignKey(Student, verbose_name='Студент')
    group = models.ForeignKey(Group, verbose_name='Группа')

class Subject(models.Model):
    "Образовательные дисциплины"
    
    name = models.CharField('Название', max_length=50)
    profile = models.ForeignKey(Profile, verbose_name='Профиль')
    desc = models.TextField('Описание', null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "дисциплину"
        verbose_name_plural = "Дисциплины"

class TestHardLevel(models.Model):
    "Уровни сложности тестов"
    
    name = models.CharField('Название', max_length=30)
    desc = models.TextField('Описание', null=True, blank=True)
   
    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'уровень сложности'
        verbose_name_plural = "Уровни сложности тестов"

class StudentsTest(models.Model):
    "Тесты по предметами"

    name = models.CharField('Название', max_length=150)
    school_class = models.PositiveSmallIntegerField('Класс')
    hard_level = models.ForeignKey(TestHardLevel, verbose_name='Уровень сложности')
    subject = models.ForeignKey(Subject, verbose_name='Дисциплина')
    desc = models.TextField('Описание', null=True, blank=True)
    time_limit = models.PositiveSmallIntegerField('Время, отведенное на тест', default=20)
    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "тест"
        verbose_name_plural = "Тесты"
    
class Question(models.Model):
    "Вопросы для учебных тестов"
    
    text = models.TextField('Текст вопроса')
    test = models.ForeignKey(StudentsTest, verbose_name='Тест')
    order = models.PositiveSmallIntegerField('Порядок в тесте')
    ball = models.PositiveSmallIntegerField('Количество баллов')
    image = models.ImageField('Изображение', upload_to='images', null=True, blank=True)

    def __unicode__(self):
        return self.text

    class Meta:
        verbose_name = "тестовый вопрос"
        verbose_name_plural = "Тестовые вопросы"

class Answer(models.Model):
    "Варианты ответов для тестовых вопросов"
    
    question = models.ForeignKey(Question, verbose_name='Вопрос')
    text = models.CharField('Текст варианта', max_length=255)
    order = models.PositiveSmallIntegerField('Порядок варианта')
    is_correct = models.BooleanField('Правильный вариант')

    def __unicode__(self):
        return self.text

    class Meta:
        verbose_name = "вариант ответа"
        verbose_name_plural = "Варианты ответов"

class TestLog(models.Model):
    "История тестирования студентов"

    student = models.ForeignKey(Student, verbose_name='Студент')
    test = models.ForeignKey(StudentsTest, verbose_name='Тест')
    testing_date = models.DateTimeField('Дата тестирования', auto_now_add=True, null=True)
    correct = models.PositiveSmallIntegerField('Правильный ответов', default=0, null=True)
    incorrect = models.PositiveSmallIntegerField('Неверных ответов', default=0, null=True)
    ball = models.PositiveSmallIntegerField('Общее количество баллов', default=0, null=True)
    trial_num = models.PositiveSmallIntegerField('Номер попытки')
  
    def __unicode__(self):
        return self.student

class QuestionLog(models.Model):
    "Подробный лог тестирования"
	
    student = models.ForeignKey(Student, verbose_name="Ученик")
    test = models.ForeignKey(StudentsTest, verbose_name="Тест")
    trial_num = models.PositiveSmallIntegerField('Номер попытки')
    question = models.ForeignKey(Question, verbose_name='Вопрос')
    selected = models.ForeignKey(Answer, verbose_name='Выбранный вариант')
    submit_date = models.DateTimeField('Дата ответа', auto_now_add=True)

    class Meta:
        verbose_name='историю тестирования'
        verbose_name_plural = 'История тестирования'
