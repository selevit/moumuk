from django.conf.urls import patterns, include, url

urlpatterns = patterns('testing.views',
    url(r'^$', 'index'),
    url(r'^begin/test-(?P<test_id>\d+)/$', 'invite_to_testing'),
    url(r'^test-(?P<test_id>\d+)/$', 'get_question_details'),
    url(r'^test-(?P<test_id>\d+)/next_question/$', 'validate_answered_question'),
    url(r'^test-(?P<test_id>\d+)/validate/$', 'testing_validate'),
)
