from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from testing.models import *
from datetime import datetime
from django.core.exceptions import *

def index(request):
    "Main page"

    return render_to_response('base/base_site.html', {
        'foo': 'baz', # TODO must be modified TODO 
    }, context_instance=RequestContext(request))

def invite_to_testing(request, test_id):
    "Invite message for student to start testing"

    current_test = StudentsTest.objects.get(id=test_id)
    
    return render_to_response('base/base_site.html', {
        'testing_invite_message': True,
        'current_test': current_test,
        'testing_time_limit': current_test.time_limit,
    }, context_instance=RequestContext(request))

def get_trial_num(current_student, current_test):
    current_trial_num = 1
    try:
        current_student_also_tested = TestLog.objects.filter (
            student=current_student,
            test=current_test).order_by('-trial_num')[0]

        if current_student_also_tested:
            current_trial_num += current_student_also_tested.trial_num
    except:
        pass
    return current_trial_num

def get_test_length(test):
    return Question.objects.filter(test=test).count()

def get_current_student(student_id):
    return Student.objects.get(id=student_id)

def get_current_test(test_id):
    return StudentsTest.objects.get(id=test_id)

def current_question_is_last(question_num, test):
    if question_num is get_test_length(test):
        return True
    return False

def write_answer_request_to_db(request, current_trial_num, student, test, answered_question):
    try:
        if request.POST['answer']:
            if answered_question: 
                p_answer = Answer.objects.get(question=answered_question,
                                              order=int(request.POST['answer']))

                pq_log = QuestionLog(student=student, test=test, 
                                     question=answered_question, selected=p_answer,
                                     trial_num=current_trial_num, submit_date=datetime.now())
                pq_log.save()
    except KeyError:
        return redirect_to_next_question(test, current_trial_num)

def get_last_answered_question(current_student, current_test, current_trial_num):
    "Get the last question of test which already answered by student"

    try:
        return QuestionLog.objects.filter(student=current_student, test=current_test, 
                trial_num=current_trial_num).order_by('-id')[0].question
    except (ObjectDoesNotExist, IndexError):
        return 0

def get_last_non_answered_question(current_student, current_test, current_trial_num):
    '''
        Get the last question of test,
        which has't been answered by student.
    '''

    last_answered_question = get_last_answered_question(current_student, current_test,
                current_trial_num)
#TODO --->
    if last_answered_question is 0:
        last_answered_question_number = 0 
#I don't like it  <---// TODO
    else:
        last_answered_question_number = last_answered_question.order

    try:
        last_non_answered_question = Question.objects.get(
                test=current_test,
                order=last_answered_question_number + 1)
    except ObjectDoesNotExist:
        return False

    return last_non_answered_question

def question_already_answered(current_question, current_trial_num):
    '''
        If current question is already answered, 
        return True, else return False
    '''
    try:
        current_question_is_already_answered = QuestionLog.objects.get(
                question=current_question, trial_num=current_trial_num)

    except ObjectDoesNotExist:
        return False

    return True

def get_question_object(current_test, question_id):
    "Get Question object"

    return Question.objects.get(test=current_test, order=question_id)

def redirect_to_next_question(current_test, current_trial_num):
    "Make http response redirect to next non-answered question"

    return redirect('/testing/test-' + str(current_test.id) + '/')

def validate_answered_question(request, test_id):
    "Validate question answered by current student"

    current_test = get_current_test(test_id)
    current_student = get_current_student(1)
    current_trial_num = get_trial_num(current_student, current_test)

    try:
        current_question_number = int(request.POST['question']) 
    except KeyError:
        return redirect_to_next_question(current_test, current_trial_num) 

    previous_answered_question = get_question_object(current_test, current_question_number)

    if not question_already_answered(previous_answered_question, current_trial_num):
        write_answer_request_to_db(request, current_trial_num,
                current_student, current_test, previous_answered_question)

    return redirect_to_next_question(current_test, current_trial_num) 

def get_question_answers(current_question):
    "Get answer variants for some question"

    try:
        return Answer.objects.filter(question=current_question).order_by('order')
    except ObjectDoesNotExist:
        return False

def display_question(request, current_question):
    "Testing for pupils"
    
#    current_student = get_current_student(1)
#    current_test = get_current_test(test_id)
#    current_test_length = get_test_length(current_test)
#    current_question_number = int(question_id)
#    current_question = get_current_question(current_test, current_question_number)
#    current_trial_num = get_trial_num(current_student, current_test)

#    if question_already_answered(current_question, current_trial_num):
#        current_question = get_last_non_answered_question(current_test, current_trial_num)
#        return redirect('/testing/test-' + str(current_test.id) + '/question-' + str(current_question.order) + '/')

#    last_question = current_question_is_last(current_question_number, current_test)
#
#    try:
#        if request.POST['question']:
#            previous_question_number = int(request.POST['question'])
#            write_answer_request_to_db(request, current_trial_num, current_student, current_test, previous_question_number)
#    except KeyError:
#        pass #TODO Boolshit. Change.
#    try:
#        current_answers = Answer.objects.filter(question=current_question).order_by('order')
#    except:
#        raise Http404 #Http404 #TODO fucking shit. I must to change it.
#

    current_test = current_question.test
    this_question_answers = get_question_answers(current_question)

    return render_to_response('base/base_site.html', {
    #    'last_question': last_question,
        'current_test': current_test,
        'current_question': current_question,
        'this_question_answers': this_question_answers,
    }, context_instance=RequestContext(request))

def get_question_details(request, test_id):
    "Testing begin for current student"
    
    current_test = get_current_test(test_id)
    current_student = get_current_student(1)
    current_trial_num = get_trial_num(current_student, current_test)
    current_question = get_last_non_answered_question(current_student, current_test, current_trial_num)
    
    if current_question is False:
        return testing_validate(request, current_student, current_test, current_trial_num)

    return display_question(request, current_question)
    

def testing_validate(request, current_student, current_test, current_trial_num):
    
    current_test_length = get_test_length(current_test)

    all_answered_questions = QuestionLog.objects.filter(student=current_student,
                                                        test=current_test,
                                                        trial_num=current_trial_num)
    a_correct_num = 0
    balls_summed = 0

    for current_answered in all_answered_questions:
        if current_answered.selected.is_correct:
            a_correct_num += 1
            balls_summed += current_answered.selected.question.ball

    ct_log = TestLog(student=current_student,
                     test=current_test,
                     correct=a_correct_num,
                     incorrect=current_test_length - a_correct_num,
                     ball=balls_summed,
                     trial_num=current_trial_num)
    ct_log.save()

    return render_to_response('base/base_site.html', {
            'testing_finished': True,
            'answered_correctly': a_correct_num,
            'sizeof_test': current_test_length,
            'balls_summed': balls_summed,
    }, context_instance=RequestContext(request))
